ARG PHP_VERSION="8.0"

FROM "wodby/drupal-php:$PHP_VERSION"

RUN wget https://get.symfony.com/cli/installer -O - | bash

RUN mv /home/wodby/.symfony5/bin/symfony /usr/local/bin/symfony